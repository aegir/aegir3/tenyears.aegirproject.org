Drutch Drupal platform
======================

This platform contains a collection of commonly used Drupal modules and themes. And Drupal core.

Which modules?
* see sites/all/modules/

Contact: http://www.drutch.nl & info@drutch.nl


Update a site
------

```
  git remote add upstream git@git.drutch.nl:drutch/drutch_core_8.git
  git fetch upstream
  git merge upstream/8.x.x-1.x --allow-unrelated-histories -X theirs

  # Fix files added in both trees
  git status | grep 'both added' | cut -d:  -f 2 | xargs -I{} sh -c "git checkout --theirs {}; git add {}"

  git commit -m 'Merge drutch 8.x platform - Drupal core 8.9.1'
```

Update the platform itself
------

```
  git remote add upstream git://git.drupalcode.org/project/drupal.git
  git fetch upstream
  git merge --squash --allow-unrelated-histories 8.1.1 -X theirs

  # Fix files added in both trees
  git status | grep 'both added' | cut -d:  -f 2 | xargs -I{} sh -c "git checkout --theirs {}; git add {}"
  git commit -m 'Merge Drupal 8.1.1'

  composer install
  git add -A
  git commit -m 'Run composer install'
```
